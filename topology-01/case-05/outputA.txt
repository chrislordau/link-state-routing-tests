I am Router A
Least cost path to router B:AFDCB and the cost is 5.6
Least cost path to router C:AFDC and the cost is 4.5
Least cost path to router D:AFD and the cost is 2.9
Least cost path to router E:AFDE and the cost is 5.8
Least cost path to router F:AF and the cost is 2.2

I am Router A
Least cost path to router B:AB and the cost is 6.5
Least cost path to router D:ABD and the cost is 10.7

I am Router A
Least cost path to router B:AFDCB and the cost is 5.6
Least cost path to router C:AFDC and the cost is 4.5
Least cost path to router D:AFD and the cost is 2.9
Least cost path to router E:AFDE and the cost is 5.8
Least cost path to router F:AF and the cost is 2.2

