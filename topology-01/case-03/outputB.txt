I am Router B
Least cost path to router A:BCDFA and the cost is 5.6
Least cost path to router C:BC and the cost is 1.1
Least cost path to router D:BCD and the cost is 2.7
Least cost path to router E:BE and the cost is 3.2
Least cost path to router F:BCDF and the cost is 3.4

I am Router B
Least cost path to router A:BA and the cost is 6.5
Least cost path to router C:BC and the cost is 1.1
Least cost path to router E:BE and the cost is 3.2
Least cost path to router F:BAF and the cost is 8.7

I am Router B
Least cost path to router A:BCDFA and the cost is 5.6
Least cost path to router C:BC and the cost is 1.1
Least cost path to router D:BCD and the cost is 2.7
Least cost path to router E:BE and the cost is 3.2
Least cost path to router F:BCDF and the cost is 3.4

