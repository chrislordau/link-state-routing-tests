## Link State Routing tests
**University of New South Wales - Term 2, 2019**

Test cases for COMP3331/9331 Link State Routing Assignment

### Important
- **Output is not guaranteed to be correct**. If you get different results please [report it on the forum](https://webcms3.cse.unsw.edu.au/COMP3331/19T2/forums/2732914) so it can be discussed amongst the students/instructors.

### Usage
* Each topology folder contains:
    * Image of the network graph with weights
    * The config files for all routers in the network
    * Test cases for the topology
* Each test case folder contains:
    * The expected console output at each router

### Test cases
#### Topology 1 (assignment spec example)

![Topology 1](topology-01/topology-01.png)

**Case 01** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-01/case-01/)
```
  0 seconds   Up (A,B,C,D,E,F)
 45 seconds   Down (A,B,C,D,E,F)
```

**Case 02** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-01/case-02/)
```
  0 seconds   Up (A,B,C,D,E,F)
 45 seconds   Down (D)
 75 seconds   Down (A,B,C,E,F)
```

**Case 03** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-01/case-03/)
```
  0 seconds   Up (A,B,C,D,E,F)
 45 seconds   Down (D)
 75 seconds   Up (D)
110 seconds   Down (A,B,C,E,F)
115 seconds   Down (D)
```

**Case 04** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-01/case-04/)
```
  0 seconds   Up (A,B,C,D,E,F)
 45 seconds   Down (C,F)
 75 seconds   Up (C,F)
110 seconds   Down (A,B,D,E)
115 seconds   Down (C,F)
```

**Case 05** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-01/case-05/)
```
  0 seconds   Up (A,B,C,D,E,F)
 45 seconds   Down (C,E,F)
 75 seconds   Up (C,E,F)
110 seconds   Down (A,B,D)
115 seconds   Down (C,E,F)
```

#### Topology 2

![Topology 2](topology-02/topology-02.png)

**Case 01** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-02/case-01/)
```
  0 seconds   Up (A,B,C,D,E,F,G,H,I,J)
 45 seconds   Down (A,B,C,D,E,F,G,H,I,J)
```

**Case 02** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-02/case-02/)
```
  0 seconds   Up (A,B,C,D,E,F,G,H,I,J)
 45 seconds   Down (E,I,J)
 75 seconds   Down (A,B,C,D,F,G,H)
```

**Case 03** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-02/case-03/)
```
  0 seconds   Up (A,B,C,D,E,F,G,H,I,J)
 45 seconds   Down (E,I,J)
 75 seconds   Up (E,I,J)
110 seconds   Down (A,B,C,D,F,G,H)
115 seconds   Down (E,I,J)
```

#### Topology 3

![Topology 3](topology-03/topology-03.png)

**Case 01** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-03/case-01/)
```
  0 seconds   Up (A,B,C,D,E,F,G,H,I,J)
 45 seconds   Down (A,B,C,D,E,F,G,H,I,J)
```

**Case 02** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-03/case-02/)
```
  0 seconds   Up (A,B,C,D,E,F,G,H,I,J)
 45 seconds   Down (E,G,J)
 75 seconds   Down (A,B,C,D,F,H,I)
```

**Case 03** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-03/case-03/)
```
  0 seconds   Up (A,B,C,D,E,F,G,H,I,J)
 45 seconds   Down (E,G,J)
 75 seconds   Up (E,G,J)
110 seconds   Down (A,B,C,D,F,H,I)
115 seconds   Down (E,G,J)
```

**Case 04** - [Output](https://bitbucket.org/chrislordau/link-state-routing-tests/src/master/topology-03/case-04/)
```
  0 seconds   Up (A,B,C,D,E,F,G,H,I,J)
 45 seconds   Down (A,D,H)
 75 seconds   Up (A,D,H)
110 seconds   Down (B,C,E,F,G,I,J)
115 seconds   Down (A,D,H)
```

### Contributing
If you want add your own tests to the repository you'll need to create a bitbucket account, fork the repository, create a new branch, make/commit your changes then send a pull-request. Bitbucket has a guide here: https://confluence.atlassian.com/bitbucket/clone-and-make-a-change-on-a-new-branch-774243398.html.
