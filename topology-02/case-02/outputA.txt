I am Router A
Least cost path to router B:AB and the cost is 4.3
Least cost path to router C:AGC and the cost is 16.5
Least cost path to router D:AGD and the cost is 17.5
Least cost path to router E:ABFIE and the cost is 14.2
Least cost path to router F:ABF and the cost is 13.0
Least cost path to router G:AG and the cost is 8.1
Least cost path to router H:ABFIH and the cost is 14.6
Least cost path to router I:ABFI and the cost is 13.2
Least cost path to router J:AJ and the cost is 6.7

I am Router A
Least cost path to router B:AB and the cost is 4.3
Least cost path to router C:AGC and the cost is 16.5
Least cost path to router D:AGD and the cost is 17.5
Least cost path to router F:ABF and the cost is 13.0
Least cost path to router G:AG and the cost is 8.1
Least cost path to router H:ABFH and the cost is 22.7

